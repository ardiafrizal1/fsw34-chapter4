let name = 'sabrina';
let pesan = 'mari belajar javascript';

console.log('tolong sampaikan $ kepada ${name}');

let price = 50;
console.log(typeof price);

//Boolean -> true & false
if (true) {
  console.log('Nice');
}

let biodata = {
  name: 'sabrina',
  hobi: ['nonton', 'belajar', 'ngoding'],
  ttl: '1 Juni 2023',
};

let fruit = ['Apple', 'Orange', 'Manggo'];

console.log(biodata.ttl);
console.log(fruit[2]);

const animals = ['cat', 'dog', ' rabbit'];
animals.push('Bear');
console.log(animals);

const animal = ['Cat', 'Dog'];
animal.pop();
console.log(animals);

let luasLingkaran;

animals.forEach(function (item) {
  console.log(item);
});
