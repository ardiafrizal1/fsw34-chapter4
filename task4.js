// #1 Gunakan metode untuk membalikan isi array

let fruits = ['Apple', 'Banana', 'Papaya', 'Grape', 'Cherry', 'Peach'];
console.log(fruits);
fruits.reverse();
console.log(fruits);
/* 
Output: 
Peach
Cherry
Grape
Papaya
Banana
Apple
*/

// #2 Gunakan metode untuk menyisipkan elemen baru dalam array

let month = ['January', 'February', 'July', 'August', 'September', 'October', 'November', 'December'];
month.shift('January');
month.shift('February');
//console.log(month);
month.unshift('January', 'February', 'March', 'April', 'Mei');
console.log(month);

/* 
Output: 
 ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
*/

// #3 Mengambil beberapa kata terakhir dalam string
const message = 'Sampaikan pada Sabrina belajar Javascript sangat menyenangkan';
console.log(message.substring(23, 60));
/* 
Output: belajar Javascript menyenangkan
*/

// #4 Buat agar awal kalimat jadi kapital
const message1 = 'Sampaikan pada Sabrina belajar Javascript sangat menyenangkan';
console.log(message1.substring(23, 60));
console.log(message1.toLocaleUpperCase(1, 1));
/* 
Output: Belajar Javascript menyenangkan
*/

/* #5 Buat function untuk menghitung BMI (Body Mass Index)
- BMI = mass / height ** 2 = mass / (height * height) (mass in kg and height in meter)
- Steven weights 78 kg and is 1.69 m tall. Bill weights 92 kg and is 1.95 
m tall
*/
function menghitungBMI() {
  let mass = 78;
  let height = 169;
  let mass2 = 92;
  let height2 = 195;

  BMI = mass / height ** 2;

  console.log('Steven weights = ' + mass, 'kg and is ' + height, 'm tall. Bill weights ' + mass2, 'kg and is ' + height2);
}
menghitungBMI();

/* #6 Menghitung BMI dengan if else statement
- John weights 95 kg and is 1.88 m tall. Nash weights 85 kg and is 1.76 
m tall

Output example:
John's BMI (28.3) is higher than Nash's (23.5)
*/
function menghitungBMI2() {
  let mass1 = 95;
  let height1 = 188;
  let mass2 = 85;
  let height2 = 176;

  BMI1 = (mass1 / height1 ** 2) * 10000;
  BMI2 = (mass2 / height2 ** 2) * 10000;

  console.log('John s BMI = ' + BMI1, 'is higher than Nashs ' + BMI2);
}
menghitungBMI2();
//  #7 Looping

let data = [10, 20, 30, 40, 50];
let total = 0;

for (i = 0; i < data.length; i++) {
  total += data[i];
}
/* You code here (you are allowed to reassigned the variable) 
Maybe you can write 3 lines or more
Use for, do while, while for, or forEach
*/
console.log(total);
console.log(`Jumlah total = ${total}`);

/* 
Jumlah total = 150
*/
